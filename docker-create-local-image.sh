#!/bin/sh

docker run -d -p 6379:63679 --name configuration_db redis
docker build -t brainy/config-backend-dev .
docker run -it -p 8080:8080 --link configuration_db --name config_backend_dev -v $(pwd):/var/www brainy/config-backend-dev 