# Brainy Service

## How to run with docker

1. Open a bash terminal with a docker machine running.
> That is:
> 1. In Linux just open the terminal.
> 1. In Mac and Windows open the "Docker Quick Start Terminal".

1. Create the docker image and container with the `Dockerfile` provided on this project (execute it on the project root folder).
```console
. docker-create-local-image.sh
```

1. Then in the same folder create your docker container based on the image created on the previous step.
```console
. docker-start-local-container.sh
```
Voilà! You are now inside the created container.

1. Initialize the application.
```console
./gradlew bootRun
```

1. Open http://localhost:8000 (Linux) or http://192.168.99.100:8000 (Mac or Windows) in your favorite browser.
