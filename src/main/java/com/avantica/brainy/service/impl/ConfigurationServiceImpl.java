package com.avantica.brainy.service.impl;

import com.avantica.brainy.beans.Configuration;
import com.avantica.brainy.dao.ConfigurationDAO;
import com.avantica.brainy.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("configurationService")
public class ConfigurationServiceImpl implements ConfigurationService {

    @Autowired
    @Qualifier("persistenceConfigurationDAO")
    private ConfigurationDAO configurationDAO;

    @Override
    public List<Configuration> getAllConfigurations() {
        return configurationDAO.getAllConfigurations();
    }

    @Override
    public boolean updateAllConfiguration(List<Configuration> configurationList) {
        return configurationDAO.updateAllConfiguration(configurationList);
    }

}
