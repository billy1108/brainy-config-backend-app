package com.avantica.brainy.dao;

import com.avantica.brainy.beans.Configuration;

import java.util.List;

public interface ConfigurationDAO {

    boolean updateAllConfiguration(List<Configuration> configurationList);

    List<Configuration> getAllConfigurations();

}
