package com.avantica.brainy.dao.impl;

import com.avantica.brainy.beans.Configuration;
import com.avantica.brainy.dao.ConfigurationDAO;
import com.avantica.brainy.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository("persistenceConfigurationDAO")
public class PersistenceConfigurationDAO implements ConfigurationDAO{

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public boolean updateAllConfiguration(List<Configuration> configurationList) {
        Map<Object, Object> configurations = new HashMap<>();
        configurationList.forEach(configuration -> configurations.put(configuration.getKey(), configuration.getValue()));
        redisTemplate.delete(Constant.CONFIGURATION_KEY);
        redisTemplate.boundHashOps(Constant.CONFIGURATION_KEY).putAll(configurations);
        return true;
    }

    @Override
    public List<Configuration> getAllConfigurations() {
        Map<Object, Object> configurations = redisTemplate.boundHashOps(Constant.CONFIGURATION_KEY).entries();
        List<Configuration> configurationList = new ArrayList<>();
        configurations.entrySet().stream().forEach(
                (Map.Entry pair) -> configurationList.add(new Configuration((String)pair.getKey(), (String)pair.getValue()))
        );

        return configurationList;
    }

}
