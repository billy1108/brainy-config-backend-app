package com.avantica.brainy.beans;


public class Message{

    private String description;

    public Message(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Message{" +
                "description='" + description + '\'' +
                '}';
    }
}